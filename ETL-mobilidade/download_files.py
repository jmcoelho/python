# -*- coding: utf-8 -*-
"""
Created on Thu Nov 05 20:43:08 2015

@author: Ana Beatriz
"""
import datetime
from urllib2 import urlopen
import re

url = 'http://sel.ic.uff.br/bus/'
urlpath =urlopen(url)
string = urlpath.read().decode('utf-8')

#pattern = re.compile('/\.[a-z]+$') #the pattern actually creates duplicates in the list
pattern = re.compile('[0-9][0-9][0-9][0-9]-[0-9][0-9]-[0-9][0-9].zip')

filelist = pattern.findall(string)
filelist = list(set(filelist))
#print(filelist)

for filename in filelist:
    if "2014" in filename:
        print "if [ ! -f " + filename + " ]"
        print "then"
        print "wget " + url + filename
        print "fi"
#    if filename == "2014-06-05.zip":
#    print "Importação iniciada em: " + str(datetime.datetime.now())
#    remotefile = urlopen(url + filename)
#    localfile = open("Data/" + filename,'wb')
#    localfile.write(remotefile.read())
#    localfile.close()
#    remotefile.close()   
#    print "Importação finalizada em: " + str(datetime.datetime.now())
        
    