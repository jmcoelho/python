# -*- coding: utf-8 -*-
"""
Created on Wed Nov 18 10:05:44 2015

@author: Ana Beatriz
"""

import json
import time
import os
import datetime
import csv
import re

log_file = open("log-json2csv", "w")
start_dttm = datetime.datetime.now()
log_file.write("STARTED AT: " + str(start_dttm) + "/n")

#directory = 'C:/Users/Ana Beatriz/Documents/cefet/mestrado/mineracao-dados/Data'
#newdirectory = 'C:/Users/Ana Beatriz/Documents/cefet/mestrado/mineracao-dados/ETL'
directory = '/home/mobility'
newdirectory = '/home/mobility/ETL'
dt = []

if not os.path.exists(newdirectory):
    os.makedirs(newdirectory)
    
date = ""
# Get all folders inside the directory
for i in os.walk(directory):
    # In each folder, read all json files

    dirname = os.path.split(i[0])[1]
    #print dirname
    
    # When we get all files in a directory, we get also the own directory. 
    # Therefor, we check if it's not the own directory
    #if (os.path.split(i[0])[1] != 'Data') & (os.path.split(i[0])[1] != 'ETL'):
    pattern = re.compile('[0-9][0-9][0-9][0-9]-[0-9][0-9]-[0-9][0-9]')
    
    if pattern.match(dirname):
        date = dirname
        if not os.path.exists(newdirectory + "/" + date):
            os.makedirs(newdirectory + "/" + date)
    
    if date != "" :
        innerdir = directory + '/' + dirname
                
        csvfilename = newdirectory + '/' + date + '/bus_data.csv'
        with open(csvfilename, 'a') as myfile:
            wr = csv.writer(myfile, quoting=csv.QUOTE_ALL, lineterminator='\n')
            print 'CSV CREATED'
        
            for (dirpath, dirnames, filenames) in os.walk(innerdir):
                                                
                for j in filenames:
                    filename = j.split('.')[0]
                    ext = j.split('.')[1]
                    if ext == 'json':
                        #print 'Reading ' + filename
                        # Get date and time data were colected
                        filedatetime = date + ' ' + filename.replace('-', ':') + ':00'
                           
                        d = datetime.datetime.strptime(filedatetime, '%Y-%m-%d %H:%M:%S') + datetime.timedelta(minutes=-10)

                        #Get the date from current folder
                        d1 = datetime.datetime.strptime(dirname, '%Y-%m-%d')

                        #In case the date less 10 minutes of tolerence results in a different day, we keep the first hour of current folder's date
                        if d.date() < d1.date():
                            d = datetime.datetime.strptime(dirname + " 00:00:00", '%Y-%m-%d %H:%M:%S')

                        try:
                            with open(dirpath.replace("\\", "/") + "/" + j) as data_file:
                               
                                data = json.load(data_file)
                                   
                                arrdata = data["DATA"]
                                for data in arrdata:
                                    if type(data[2]) is float:
                                        data[2] = str(int(data[2]))
                                       
                                    dtdatetime = time.strftime(data[0])
                                    dtdatetime = datetime.datetime.strptime(dtdatetime, '%m-%d-%Y %H:%M:%S')
                                    if dtdatetime >= d :
                                        wr.writerow(data)
                        except Exception as e:
                            #Uncomment the line bellow to see the exception details
                            #print repr(e)
                            log_file.write("Exception found in " + dirpath + "/" + filename + ": " repr(e))
                            continue
                        

log_file.write("FINISHED AT: " + str(datetime.datetime.now()) + "/n")
log_file.write("TIME TAKEN: " + str(datetime.datetime.now() - start_dttm) + "/n")
log_file.close()
