from datetime import date
import os
import re

d0 = date(2014, 4, 16)
d1 = date(2014, 12, 31)
delta = d1 - d0
print delta.days

div = delta//4
div = div.days

print div

directory = '/home/mobility/ETL'
dt = []
k = 1
s = 1

dirlist = sorted(os.walk(directory))

for i in dirlist:
    if (re.match("\d+-\d+-\d+", os.path.split(i[0])[1])):
        #if not os.path.exists(directory + "/" + os.path.split(i[0])[1] + "/bus_treated_data.csv"):
                if (s <= 4):
                    if (k <= div):
                        
                        if (k == 1):
                            filename = "bash" + str(s) + ".sh"
                            #print filename
                            bash_file = open(filename, "w")
                            bash_file.write("#!/bin/bash \n")
                            
                        filepath = directory + "/" + os.path.split(i[0])[1]
                        
                        if os.path.getsize(filepath + "/bus_data.csv") > 0 :
                            print os.path.getsize(filepath + "/bus_data.csv")
                            bash_file.write("Rscript DuplicatedData.R " + filepath + "\n")
                        
                        #if os.path.exists(directory + "/" + os.path.split(i[0])[1] + "/bus_treated_data.csv"):
                            bash_file.write("psql \n")
                            #bash_file.write("\copy dinamico_olap(dia, id_onibus, id_linha, latitude, longitude, vel, latx1, longx1, distance, seq) from '" + filepath + "/bus_treated_data.tbl' delimiter '|';")
                            bash_file.write("\copy dinamico_olap(dia, id_onibus, id_linha, latitude, longitude, vel, latx1, longx1, distance, seq) from '" + filepath + "/bus_treated_data.tbl' delimiter ',' CSV HEADER;")
                            bash_file.write("\n")
                                                
                        if (k == div):
                            bash_file.close()
                            s = s + 1
                            k = 0
                        
                        #print s
                        #print k    
                        k = k + 1
                        
                        
            #print os.path.split(i[0])[1]