'''
Created on Sep 7, 2015

@author: eogasawara
'''

import csv

#csvfilename ='anais-completo.csv'
#csvfilename ='anais-short.csv'
#csvfilename ='demos-app.csv'
#csvfilename ='workshop.csv'
csvfilename ='concurso-teses.csv'


with open(csvfilename, 'rb') as csvfile:
    spamreader = csv.reader(csvfile, delimiter=',', quotechar='"')
    
    for row in spamreader:
        while 'NULL' in row:
            row.remove('NULL')
        title = row[0]
        if title == 'nome': continue
        idativ = row[1]
        filename = 'pdf/' + row[2]
        paperid = 'paper:'+idativ
        autores = ', '.join(row[3:])
        print '\\noindent ' + title +' \\dotfill \\hyperlink{' + filename + '.1}{\pageref*{'+paperid+'}}\\\\'
        print '\\textit{' +  autores + '}\\\\'
        print ''

    print '###'
    print ''

with open(csvfilename, 'rb') as csvfile:
    spamreader = csv.reader(csvfile, delimiter=',', quotechar='"')

    for row in spamreader:
        while 'NULL' in row:
            row.remove('NULL')
        title = row[0]
        if title == 'nome': continue
        idativ = row[1]
        filename = 'pdf/' + row[2]
        paperid = 'paper:'+idativ
        autores = ', '.join(row[3:])
        print '\\includepdf[pages={-}, pagecommand={},link=true, addtotoc={1, section, 1, '+title+', '+ paperid + '}]{'+filename+'}'
        
    print ''
    print '###'
    print ''

with open(csvfilename, 'rb') as csvfile:
    spamreader = csv.reader(csvfile, delimiter=',', quotechar='"')

    for row in spamreader:
        while 'NULL' in row:
            row.remove('NULL')
        title = row[0]
        if title == 'nome': continue
        idativ = row[1]
        filename = 'pdf/' + row[2]
        paperid = 'paper:'+idativ
        autores = ', '.join(row[3:])
        print idativ + '\\pageref*{'+ paperid + '}\\'
        

    